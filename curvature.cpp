#include <iostream>
#include <vector>

#include <omp.h>
#include <pcl/io/ply_io.h>
#include <pcl/point_types.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/features/principal_curvatures.h>

#include <pcl/kdtree/kdtree_flann.h>
 
// gco
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include "GCoptimization.h"


using namespace pcl;
using namespace std;

int main (int argc, char** argv)
{

// parameters
    double sigma = 0.1;             // std dev for conevrting curvature into gaussian probability
	int K = 50;                     //
    double unary_factor = 100;      // (1-prob) * factor
    double normal_radius = 0.5;     //
    double curvature_radius = 0.3;  //

 // sigma K unaryfactor  normal_radius curv_radius
  sigma = strtod(argv[2], NULL); // from curvature itself
  K = strtod(argv[3], NULL); // from curvature itself
  unary_factor = strtod(argv[4], NULL); // from curvature itself
  normal_radius = strtod(argv[5], NULL); // from curvature itself
  curvature_radius = strtod(argv[6], NULL); // from curvature itself
  
  std::string fileName = argv[1];
  std::cout << "Reading " << fileName << std::endl;

  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);

  if (pcl::io::loadPLYFile<pcl::PointXYZ> (fileName, *cloud) == -1) //* load the file
  {
    PCL_ERROR ("Couldn't read file");
    return (-1);
  }

  std::cout << "Loaded " << cloud->points.size() << " points." << std::endl;

  // Compute the normals
  pcl::NormalEstimationOMP<pcl::PointXYZ, pcl::Normal> normalEstimation;
  normalEstimation.setInputCloud (cloud);

  pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);
  normalEstimation.setSearchMethod (tree);

  pcl::PointCloud<pcl::Normal>::Ptr cloudWithNormals (new pcl::PointCloud<pcl::Normal>);
  normalEstimation.setRadiusSearch (normal_radius);
  normalEstimation.compute (*cloudWithNormals);

  std::cout << "Computed normals." << std::endl;


  // Setup the principal curvatures computation
  pcl::PrincipalCurvaturesEstimation<pcl::PointXYZ, pcl::Normal, pcl::PrincipalCurvatures> principalCurvaturesEstimation;

  // Provide the original point cloud (without normals)
  principalCurvaturesEstimation.setInputCloud (cloud);

  // Provide the point cloud with normals
  principalCurvaturesEstimation.setInputNormals(cloudWithNormals);

  // Use the same KdTree from the normal estimation
  principalCurvaturesEstimation.setSearchMethod (tree);
  principalCurvaturesEstimation.setRadiusSearch(curvature_radius);

  // Actually compute the principal curvatures
  pcl::PointCloud<pcl::PrincipalCurvatures>::Ptr principalCurvatures (new pcl::PointCloud<pcl::PrincipalCurvatures> ());
  principalCurvaturesEstimation.compute (*principalCurvatures);
  std::cout << "Computed curvatures." << std::endl;



  std::cout << "output points.size (): " << principalCurvatures->points.size () << std::endl;

  // Display and retrieve the curvature descriptor vector for the 0th point.
  //  p.principal_curvature[0] << "," << p.principal_curvature[1] << "," << p.principal_curvature[2] << " - " << p.pc1 << "," << p.pc2 << ")";
  // x y z pc1 pc2 
  // pc1, pc2 == max/min curvature
  // xyz = principle direction              if(pc1!=pc2) 
  // pc1*pc2 = gaussian curvature k
  // (pc1+pc2)/2 = mean curvature
   
  pcl::PrincipalCurvatures descriptor = principalCurvatures->points[0];
//  std::cout << descriptor << std::endl;
  
  
  
   for (int idx = 0; idx < principalCurvatures->points.size(); idx++)
  {
      cout << principalCurvatures->points[idx].pc1 << " " <<  principalCurvatures->points[idx].pc2 << " "
        << principalCurvatures->points[idx].principal_curvature[0] << " "
        << principalCurvatures->points[idx].principal_curvature[1] << " "
        << principalCurvatures->points[idx].principal_curvature[2] << endl;
   }
        
  exit(0);
   
  pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr point_cloud_ptr_max (new pcl::PointCloud<pcl::PointXYZRGBNormal>);
  pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr point_cloud_ptr_min (new pcl::PointCloud<pcl::PointXYZRGBNormal>);


  for (int idx = 0; idx < principalCurvatures->points.size(); idx++)
  {
      uint8_t r(0), g(0), b(0);
                   
      double pc1 = principalCurvatures->points[idx].pc1; // max 
      double pc2 = principalCurvatures->points[idx].pc2; // min
   
      std::cout << "min=" << pc2 << " max=" << pc1  << std::endl;
            
      pcl::PointXYZRGBNormal point;
      point.x = cloud->points[idx].x;
      point.y = cloud->points[idx].y;
      point.z = cloud->points[idx].z;

      point.normal_x = cloudWithNormals->points[idx].normal_x;
      point.normal_y = cloudWithNormals->points[idx].normal_y;
      point.normal_z = cloudWithNormals->points[idx].normal_z;

      // todo: more complex coloring for 4 classes
      r = pc1*2550;
      g = 0;
      b = 0;
      
      // convert and push colored point    
      uint32_t rgb = (static_cast<uint32_t>(r) << 16 |  static_cast<uint32_t>(g) << 8 | static_cast<uint32_t>(b));
      point.rgb = *reinterpret_cast<float*>(&rgb);
      point_cloud_ptr_max->points.push_back (point);

      // todo: more complex coloring for 4 classes
      r = pc2*2550;
      g = 0;
      b = 0;
      
      // convert and push colored point    
      rgb = (static_cast<uint32_t>(r) << 16 |  static_cast<uint32_t>(g) << 8 | static_cast<uint32_t>(b));
      point.rgb = *reinterpret_cast<float*>(&rgb);
      point_cloud_ptr_min->points.push_back (point);
  }
  
  pcl::PLYWriter writer;
  writer.write<PointXYZRGBNormal> ("curvature_classes_min.ply", *point_cloud_ptr_min, false);
  printf("written curvature_classes.ply\n");
  
  writer.write<PointXYZRGBNormal> ("curvature_classes_max.ply", *point_cloud_ptr_max, false);
  printf("written curvature_classes.ply\n");
  
  
  
  
  pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr point_cloud_ptr (new pcl::PointCloud<pcl::PointXYZRGBNormal>);

        // gco
    int  num_sites = principalCurvatures->points.size();
    int  num_labels = 4;
    int *result = new int[num_sites];   // stores result of optimization
	int *data = new int[num_sites*num_labels];


  uint8_t r(0), g(0), b(0);

  int idx = 0;
  for (idx = 0; idx < principalCurvatures->points.size(); idx++)
  {
                   
      double pc1 = principalCurvatures->points[idx].pc1;
      double pc2 = principalCurvatures->points[idx].pc2;
      
      double mincurv = exp(-(pc2*pc2) / (2*sigma*sigma));
      double maxcurv = exp(-(pc1*pc1) / (2*sigma*sigma));
      
      double class1 = mincurv * maxcurv;         // planar              (blue in Fig 2 bmvc2009) 
      double class2 = mincurv * (1-maxcurv);     // developable convex  (red in Fig 2 bmvc2009) 
      double class3 = (1-mincurv) * maxcurv;     // developable concave (green in Fig 2 bmvc2009) 
      double class4 = (1-mincurv) * (1-maxcurv); // non developable     (yellow in Fig 2 bmvc2009) 
 
      //std::cout << "class1=" << class1 << " class2=" << class2  << " class3=" << class3  << " class4=" << class4 << std::endl;
  

      pcl::PointXYZRGBNormal point;
      point.x = cloud->points[idx].x;
      point.y = cloud->points[idx].y;
      point.z = cloud->points[idx].z;

      point.normal_x = cloudWithNormals->points[idx].normal_x;
      point.normal_y = cloudWithNormals->points[idx].normal_y;
      point.normal_z = cloudWithNormals->points[idx].normal_z;

      // todo: more complex coloring for 4 classes
      r = class2*255;
      g = class3*255;
      b = class1*255;
      
      // convert and push colored point    
      uint32_t rgb = (static_cast<uint32_t>(r) << 16 |  static_cast<uint32_t>(g) << 8 | static_cast<uint32_t>(b));
      point.rgb = *reinterpret_cast<float*>(&rgb);
      point_cloud_ptr->points.push_back (point);
      
      // gcut data costs
		data[idx*num_labels+0] = (1-class1)*unary_factor;
		data[idx*num_labels+1] = (1-class2)*unary_factor;
		data[idx*num_labels+2] = (1-class3)*unary_factor;
		data[idx*num_labels+3] = (1-class4)*unary_factor;
  }
  

  writer.write<PointXYZRGBNormal> ("curvature_classes.ply", *point_cloud_ptr, false);
  printf("written curvature_classes.ply\n");
  
	// next set up the array for smooth costs
	int *smooth = new int[num_labels*num_labels];
	for ( int l1 = 0; l1 < num_labels; l1++ )
		for (int l2 = 0; l2 < num_labels; l2++ )
		{
			smooth[l1+l2*num_labels] = (l1-l2)*(l1-l2) <= 4  ? (l1-l2)*(l1-l2):4;
			cout << "smooth" << 			smooth[l1+l2*num_labels] << endl; 
			}


    // gco graph
		GCoptimizationGeneralGraph *gc = new GCoptimizationGeneralGraph(num_sites,num_labels);
		gc->setDataCost(data);
		//gc->setSmoothCost(smooth);

		// now set up a grid neighborhood system
		// first set up horizontal neighbors
        pcl::KdTreeFLANN<pcl::PointXYZ> kdtree;
        kdtree.setInputCloud (cloud);
  		
        std::vector<int> pointIdxNKNSearch(K);
        std::vector<float> pointNKNSquaredDistance(K);

        //  todo move above
        for (int idx = 0; idx < principalCurvatures->points.size(); idx++)
        {
             kdtree.nearestKSearch (cloud->points[idx], K, pointIdxNKNSearch, pointNKNSquaredDistance);
             // todo weigh by distance
             for (int nnidx = 0; nnidx < K; nnidx++)
             {
             // cout << "idx=" << idx << ": " << pointIdxNKNSearch[nnidx] << endl;
                 gc->setNeighbors(idx,pointIdxNKNSearch[nnidx]);
                 }
        }
          
		
		printf("\nBefore optimization energy is %d\n",gc->compute_energy());
		gc->expansion(5);// run expansion for 2 iterations. For swap use gc->swap(num_iterations);
		printf("\nAfter optimization energy is %d\n",gc->compute_energy());

		for ( int  i = 0; i < num_sites; i++ )
			result[i] = gc->whatLabel(i);
	
	
  pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr point_cloud_gco_ptr (new pcl::PointCloud<pcl::PointXYZRGBNormal>);	
  
	for (int idx = 0; idx < num_sites; idx++)
	{

      pcl::PointXYZRGBNormal point;
      point.x = cloud->points[idx].x;
      point.y = cloud->points[idx].y;
      point.z = cloud->points[idx].z;

      point.normal_x = cloudWithNormals->points[idx].normal_x;
      point.normal_y = cloudWithNormals->points[idx].normal_y;
      point.normal_z = cloudWithNormals->points[idx].normal_z;


    // for debug
    //      double class1 = mincurv * maxcurv;         // planar              (blue in Fig 2 bmvc2009) 
    //      double class2 = mincurv * (1-maxcurv);     // developable convex  (red in Fig 2 bmvc2009) 
    //      double class3 = (1-mincurv) * maxcurv;     // developable concave (green in Fig 2 bmvc2009) 
    //      double class4 = (1-mincurv) * (1-maxcurv); // non developable     (yellow in Fig 2 bmvc2009) 

    //cout << "idx=" << idx << ": " << result[idx] << endl;
	switch(result[idx])
	{
	case 0:  r = 0; g = 0; b = 255; break; // blue == planar
	case 1:  r = 255; g = 0; b = 0; break; // red == developable convex
	case 2:  r = 0; g = 255; b = 255; break; // green == developable concave
	case 3:  r = 255; g = 255; b = 0; break; // yellow == non developable
    default:  r = 0; g = 0; b = 0; break; // black == unknown error
	}
          
      // convert and push colored point    
      uint32_t rgb = (static_cast<uint32_t>(r) << 16 |  static_cast<uint32_t>(g) << 8 | static_cast<uint32_t>(b));
      point.rgb = *reinterpret_cast<float*>(&rgb);
      point_cloud_gco_ptr->points.push_back (point);

	}
	
	

  writer.write<PointXYZRGBNormal> ("curvature_classes_gco.ply", *point_cloud_gco_ptr, false);
  printf("written curvature_classes_gco.ply\n");
	  
  
      delete gc;
	delete [] result;
	delete [] smooth;
	delete [] data;

  
    
  

  return 0;
}
